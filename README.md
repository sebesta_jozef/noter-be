# Noter BE - Spring boot REST backend (java 11) for noter app

### Compiling (will also create and migrate sqlite db file)

```
mvn clean install -DskipTests
```

### For DB migration you can use also command:

``
mvn liquibase:update
``

### Deployment is just through starting main class: 

com.note.NoteApplication


## Jar version of noter BE including migrated sqlite DB file is located here:

https://bitbucket.org/sebesta_jozef/noter-be-jar/src/master/

## FE (react app) is located here:

https://bitbucket.org/sebesta_jozef/noter/src/master/

