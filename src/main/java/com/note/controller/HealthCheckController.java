package com.note.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("health-check")
@RestController
public class HealthCheckController {
    @GetMapping
    public ResponseEntity getHealtCheck() {
        return ResponseEntity.ok("UP");
    }
}
