package com.note.controller;

import com.note.dto.NoteDto;
import com.note.service.NoteService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("notes")
public class NoteControllerImpl {

    private final NoteService noteService;

    @GetMapping
    public List<NoteDto> getNotes() {
        return noteService.getAllNotes();
    }

    @GetMapping({"/{id}"})
    public NoteDto getById(@PathVariable("id") final Long id) {
        return noteService.getNoteById(id);
    }

    @PostMapping
    public void createNote(@Valid @RequestBody final NoteDto noteDto, Principal principal) {
        noteService.createNote(noteDto, principal);
    }

    @PutMapping("/{id}")
    public void updateNote(@PathVariable("id") final Long id, @Valid @RequestBody final NoteDto noteDto, Principal principal) {
        noteService.updateNote(id, noteDto, principal);
    }

    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable("id") final Long id) {
        noteService.deleteNote(id);
    }

}
