package com.note.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Schema(description = "Note object")
public class NoteDto {

    @JsonProperty(value = "id")
    @ApiModelProperty(required = true, value = "Note ID")
    private Long id;

    @NotBlank
    @JsonProperty(value = "title")
    @ApiModelProperty(required = true, value = "Note title")
    private String title;

    @NotBlank
    @JsonProperty(value = "description")
    @ApiModelProperty(required = true, value = "Note description")
    private String description;

    @JsonProperty(value = "createdBy")
    @ApiModelProperty(required = true, value = "Created by")
    private String createdBy;

    @JsonProperty(value = "modifiedBy")
    @ApiModelProperty(required = false, value = "Modified by")
    private String modifiedBy;

    @JsonProperty(value = "createdAt")
    @ApiModelProperty(required = true, value = "Created at")
    private String createdAt;

    @JsonProperty(value = "modifiedAt")
    @ApiModelProperty(required = false, value = "Modified at")
    private String modifiedAt;
}
