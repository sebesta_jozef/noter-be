package com.note.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class Response implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer status;
    private ResponseError error;

    public Response(final Integer status, final ResponseError error) {
        this.status = status;
        this.error = error;
    }

    @Override
    public String toString() {
        return "Response [status=" + status + ", error=" + error + "]";
    }
}
