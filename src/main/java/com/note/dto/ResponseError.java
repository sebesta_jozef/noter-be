package com.note.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Schema(description = "error response")
@Data
public class ResponseError implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(required = false, value = "Error code")
    private final String code;

    @ApiModelProperty(required = false, value = "Error message")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public ResponseError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }
}
