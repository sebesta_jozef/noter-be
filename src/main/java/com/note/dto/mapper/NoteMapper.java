package com.note.dto.mapper;

import com.note.dto.NoteDto;
import com.note.entity.NoteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.NullValuePropertyMappingStrategy;


@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface NoteMapper {
    @Mappings({
            @Mapping(target="id", source="id"),
            @Mapping(target="title", source="title"),
            @Mapping(target="description", source="description"),
            @Mapping(target="createdBy", source="createdBy"),
            @Mapping(target="modifiedBy", source="modifiedBy", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT),
            @Mapping(target="modifiedAt", source="modifiedAt", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT, dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"),
            @Mapping(target="createdAt", source="createdAt", dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    })
    NoteDto noteEntityToNoteDto(NoteEntity noteEntity);

    @Mappings({
            @Mapping(target="title", source="title"),
            @Mapping(target="description", source="description"),
    })
    NoteEntity noteDtoToNoteEntity(NoteDto noteDTO);

    @Mappings({
            @Mapping(target="title", source="title"),
            @Mapping(target="description", source="description"),
    })
    NoteEntity updateNoteEntityFromNoteDto(@MappingTarget NoteEntity noteEntity, NoteDto noteDto);
}
