package com.note.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import java.io.Serializable;

@Data
@Entity
@Table(name = "Note")
public class NoteEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CREATED_AT", nullable = false)
    private String createdAt;

    @Column(name = "MODIFIED_AT", nullable = true)
    private String modifiedAt;

    @Column(name = "CREATED_BY", nullable = false)
    private String createdBy;

    @Column(name = "MODIFIED_BY", nullable = true)
    private String modifiedBy;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;
}
