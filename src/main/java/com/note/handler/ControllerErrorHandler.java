package com.note.handler;

import com.note.dto.Response;
import com.note.dto.ResponseError;
import com.note.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ControllerErrorHandler {

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ResponseBody
    public Response handleException(final BadCredentialsException e) {
        log.error("BadRequest exception", e);
        return new Response(HttpStatus.BAD_REQUEST.value(),
                new ResponseError("BAD_CREDENTIALS", e.getMessage()));
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public Response handleException(final ObjectNotFoundException e) {
        log.error("ObjectNotFound exception", e);
        return new Response(HttpStatus.NOT_FOUND.value(),
                new ResponseError(e.getCode().name(), e.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    @Order(Ordered.LOWEST_PRECEDENCE)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response fallback(Exception e) {
        log.error("Unexpected exception", e);
        return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new ResponseError("INTERNAL_SERVER_ERROR", e.getMessage()));
    }
}
