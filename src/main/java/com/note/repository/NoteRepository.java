package com.note.repository;

import com.note.entity.NoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NoteRepository extends JpaRepository<NoteEntity, String> {
    Optional<NoteEntity> findById(Long id);

    Optional<NoteEntity> findByTitle(String title);
}
