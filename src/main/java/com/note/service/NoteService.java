package com.note.service;

import com.note.dto.NoteDto;
import com.note.dto.mapper.NoteMapper;
import com.note.entity.NoteEntity;
import com.note.exception.ObjectNotFoundException;
import com.note.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoteService {

    private final NoteRepository noteRepository;
    private final NoteMapper noteMapper;

    public List<NoteDto> getAllNotes() {
        List<NoteDto> notes = noteRepository.findAll(Sort.by(Sort.Direction.DESC, "modifiedAt")).stream().map(note -> noteMapper.noteEntityToNoteDto(note)).collect(Collectors.toList());
        log.info("Notes successfully retrieved");
        return notes;
    }

    public NoteDto getNoteById(Long id) {
        NoteEntity noteEntity = noteRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.NOTE_NOT_FOUND, "Note not found"));
        log.info("Note successfully retrieved with ID {}", id);
        return  noteMapper.noteEntityToNoteDto(noteEntity);
    }

    public void createNote(NoteDto noteDto, Principal principal) {
        NoteEntity noteEntity = noteMapper.noteDtoToNoteEntity(noteDto);
        ZonedDateTime zonedDateTimeNow = ZonedDateTime.now(ZoneId.of("UTC"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z");
        String formattedString = zonedDateTimeNow.format(formatter);
        noteEntity.setCreatedAt(formattedString);
        noteEntity.setModifiedAt(formattedString);
        noteEntity.setCreatedBy(principal.getName());
        noteEntity.setModifiedBy(principal.getName());
        noteRepository.save(noteEntity);
        log.info("Note successfully created with ID {}", noteEntity.getId());
    }

    public void updateNote(Long id, NoteDto noteDto, Principal principal) {
        NoteEntity noteEntity = noteRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.NOTE_NOT_FOUND, "Note not found"));

        NoteEntity updatedNoteEntity = noteMapper.updateNoteEntityFromNoteDto(noteEntity, noteDto);
        ZonedDateTime zonedDateTimeNow = ZonedDateTime.now(ZoneId.of("UTC"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z");
        String formattedString = zonedDateTimeNow.format(formatter);
        updatedNoteEntity.setModifiedAt(formattedString);
        updatedNoteEntity.setModifiedBy(principal.getName());
        noteRepository.save(updatedNoteEntity);
        log.info("Note successfully updated with ID {}", updatedNoteEntity.getId());
    }

    public void deleteNote(Long id) {
        NoteEntity noteEntity = noteRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.NOTE_NOT_FOUND, "Note not found"));

        noteRepository.delete(noteEntity);
        log.info("Note successfully deleted with ID {}", noteEntity.getId());
    }
}
